import logging
import os
import ftplib
from datetime import datetime
import time
import xml.etree.ElementTree as et
from mysql import connector


def get_values_from_xml(path: str) -> dict:
    res = {}
    root = et.parse(path).getroot()
    for company in root.findall('Контрагент/CatalogObject.Контрагенты'):
        comp_dict = {}
        for item in company:
            if item.text == 'true':
                comp_dict[item.tag] = 1
            elif item.text == 'false':
                comp_dict[item.tag] = 0
            elif item.tag == 'ДатаСоздания':
                comp_dict[item.tag] = datetime.strptime(item.text, "%Y-%m-%dT%H:%M:%S")
            else:
                comp_dict[item.tag] = item.text
        res[company[4].text] = comp_dict
    return res


def company_exist(primary_key: str) -> bool:
    cursor = connection.cursor()
    cursor.execute(f"SELECT * FROM companies WHERE Code={primary_key}")
    return cursor.fetchall() != []


def retrieve_from_ftp() -> bool:
    ftp_host = ""
    ftp = ftplib.FTP()
    try:
        ftp.connect(ftp_host)
        ftp.login('', '')
        ftp.retrbinary("RETR " + '1c.xml', open('new.xml', 'wb').write)
        ftp.quit()
        return True
    except ftplib.all_errors as err:
        logger.error(f'FTP error: {err}')
        return False


def get_company_info(primary_key: str) -> dict:
    cursor = connection.cursor()
    cursor.execute(f'SELECT * FROM companies WHERE Code={primary_key}')
    return dict(zip(cursor.column_names, cursor.fetchall()[0]))


def get_all_companies() -> dict:
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM companies')
    all_companies = cursor.fetchall()
    columns = cursor.column_names
    res = {all_companies[i][4]: dict(zip(columns, all_companies[i])) for i in range(len(all_companies))}
    return res


def insert_to_table(params: str, values: tuple) -> None:
    primary_key = values[4]
    cursor = connection.cursor()
    sql = f'INSERT INTO companies ({params}) VALUES ({str("%s ," * len(values)).rstrip(" ,")})'
    try:
        cursor.execute(sql, values)
        connection.commit()
    except connector.Error as err:
        logger.error(f'{primary_key} error. {err}')


def update_table(primary_key: str, params: str, values: tuple) -> None:
    cursor = connection.cursor()
    sql = f'UPDATE companies SET {params}  WHERE Code = {primary_key}'
    try:
        cursor.execute(sql, values)
        connection.commit()
    except connector.Error as err:
        logger.error(f'{primary_key} error. {err}')


if __name__ == '__main__':
    LOG_FORMAT = "%(asctime)s - %(message)s"
    logging.basicConfig(filename='log.txt',
                        level=logging.DEBUG,
                        format=LOG_FORMAT)
    logger = logging.getLogger()

    while True:
        try:
            connection = connector.connect(
                    host='',
                    user='',
                    password='',
                    database='')
        except connector.Error as err:
            logger.error(err)
            print('Conncetion error')
            time.sleep(60)
            continue

        try:
            os.remove('new.xml')
        except FileNotFoundError:
            pass

        if retrieve_from_ftp():
            all_companies = get_all_companies()
            new_file = get_values_from_xml('new.xml')
            for key in new_file.keys():
                if key in all_companies.keys():
                    old_set = set(all_companies[key].items())
                    new_set = set(new_file[key].items())
                    diff = new_set - old_set
                    if diff:
                        params = ''.join([f'{dif[0]}=%s,' for dif in diff]).rstrip(',')
                        values = tuple([dif[1] for dif in diff])
                        update_table(key, params, values)
                        logger.info(f'Modified company with code {key}; New data: {diff}')
                else:
                    params = ','.join(list(new_file[key].keys()))
                    values = tuple(new_file[key].values())
                    insert_to_table(params, values)
                    logger.info(f'Added new entry: {values}')
            logger.info('Done. Sleep for 1 hour')
            time.sleep(3600)
        else:
            logger.error('FTP load failed')
            time.sleep(300)


